package org.fininfo.fetcher;

import org.fininfo.model.FinancialInformation;

public class FinancialInformationFetcher {

    public FinancialInformation getFinancialInformation(String customerId) {
        return mockFinancialInformation();
    }

    private FinancialInformation mockFinancialInformation() {
        return new FinancialInformation("John Doe", "New Delhi", 100000D, 1500000D,
                40000D, 5000000D, 500000D);
    }

}
