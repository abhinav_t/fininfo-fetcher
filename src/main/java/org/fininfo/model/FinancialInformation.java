package org.fininfo.model;

import java.util.Objects;

public class FinancialInformation {

    public final String name;
    public final String address;
    public final Double monthlyIncome;
    public final Double annualCTC;
    public final Double typicalMonthlyExpenses;
    public final Double lifeInsuranceCover;
    public final Double medicalInsuranceCover;

    public FinancialInformation(String name, String address, Double monthlyIncome, Double annualCTC,
                                Double typicalMonthlyExpenses, Double lifeInsuranceCover,
                                Double medicalInsuranceCover) {
        this.name = name;
        this.address = address;
        this.monthlyIncome = monthlyIncome;
        this.annualCTC = annualCTC;
        this.typicalMonthlyExpenses = typicalMonthlyExpenses;
        this.lifeInsuranceCover = lifeInsuranceCover;
        this.medicalInsuranceCover = medicalInsuranceCover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinancialInformation that = (FinancialInformation) o;
        return name.equals(that.name) &&
                address.equals(that.address) &&
                monthlyIncome.equals(that.monthlyIncome) &&
                annualCTC.equals(that.annualCTC) &&
                typicalMonthlyExpenses.equals(that.typicalMonthlyExpenses) &&
                lifeInsuranceCover.equals(that.lifeInsuranceCover) &&
                medicalInsuranceCover.equals(that.medicalInsuranceCover);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, monthlyIncome, annualCTC, typicalMonthlyExpenses, lifeInsuranceCover,
                medicalInsuranceCover);
    }

    @Override
    public String toString() {
        return "FinancialInformation{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", monthlyIncome=" + monthlyIncome +
                ", annualCTC=" + annualCTC +
                ", typicalMonthlyExpenses=" + typicalMonthlyExpenses +
                ", lifeInsuranceCover=" + lifeInsuranceCover +
                ", medicalInsuranceCover=" + medicalInsuranceCover +
                '}';
    }

}
